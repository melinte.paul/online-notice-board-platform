package com.meep.noticeboard.data.dirs;

import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Directory {
    @Id private String id;
    private String parentId;
    private List<String> childDirIds;
    private List<String> fileIds;
    private String name;

    public Directory(String parentId, List<String> childDirIds, List<String> fileIds, String name) {
        this.parentId = parentId;
        this.childDirIds = childDirIds;
        this.fileIds = fileIds;
        this.name = name;
    }

    public Directory(String name) {
        this.name = name;
        parentId = null;
        childDirIds = new ArrayList<>();
        fileIds = new ArrayList<>();
    }

    public Directory() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public List<String> getChildDirIds() {
        return childDirIds;
    }

    public void setChildDirIds(List<String> childDirIds) {
        this.childDirIds = childDirIds;
    }

    public List<String> getFileIds() {
        return fileIds;
    }

    public void setFileIds(List<String> fileIds) {
        this.fileIds = fileIds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Directory directory = (Directory) o;
        return id.equals(directory.id) && name.equals(directory.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
