package com.meep.noticeboard.data.dirs;

import com.meep.noticeboard.data.users.UserRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DirectoryController {

    private UserRepository userRepository;
    private DirectoryRepository directoryRepository;

    public DirectoryController(UserRepository userRepository, DirectoryRepository directoryRepository) {
        this.userRepository = userRepository;
        this.directoryRepository = directoryRepository;
    }

    @GetMapping("/dirs")
    List<Directory> all(){
        return directoryRepository.findAll();
    }
}
