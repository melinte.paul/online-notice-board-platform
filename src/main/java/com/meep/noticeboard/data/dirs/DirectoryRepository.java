package com.meep.noticeboard.data.dirs;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface DirectoryRepository extends MongoRepository<Directory,String> {
}
