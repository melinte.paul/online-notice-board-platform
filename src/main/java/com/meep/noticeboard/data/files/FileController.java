package com.meep.noticeboard.data.files;

import com.meep.noticeboard.data.dirs.Directory;
import com.meep.noticeboard.data.dirs.DirectoryRepository;
import com.meep.noticeboard.data.users.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FileController {

    private UserRepository userRepository;
    private FileRepository fileRepository;
    @Autowired
    private DirectoryRepository directoryRepository;

    public FileController(UserRepository userRepository, FileRepository fileRepository) {
        this.userRepository = userRepository;
        this.fileRepository = fileRepository;
    }

    @GetMapping("/files")
    List<File> all(){
        return fileRepository.findAll();
    }

    @GetMapping(value = "/files",params = {"dirId"})
    List<File> allFilesInDir(@RequestParam("dirId") String dirId){
        Directory dir = directoryRepository.findById(dirId).orElse(null);
        //TODO exception null
        return (List<File>) fileRepository.findAllById(dir.getFileIds());
    }
}
