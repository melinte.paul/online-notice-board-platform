package com.meep.noticeboard.data.users;

import com.meep.noticeboard.exceptions.UserNotFoundException;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
public class UserController {

    private UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/users")
    List<User> all() {
        return userRepository.findAll();
    }

    @GetMapping(value = "/users", params = {"name"})
    User byName(@RequestParam("name") String name) {
        User user = userRepository.findByName(name);
        if (user == null) {
            throw new UserNotFoundException("name:" + name);
        }
        return user;
    }

    @GetMapping(value = "/users", params = {"name", "pass"})
    User login(@RequestParam("name") String name, @RequestParam("pass") String pass) {
        User user = userRepository.findByName(name);
        if (user == null) {
            throw new UserNotFoundException("name:" + name);
        }
        if (user.getPassword().equals(pass)) {
            return user;
        } else return null;

    }

    @GetMapping(value = "/users", params = {"id"})
    User byId(@RequestParam("id") String id) {
        User user = userRepository.findById(id).orElse(null);
        if (user == null) {
            throw new UserNotFoundException("id:" + id);
        }
        return user;
    }

    @PostMapping(value = "/users")
    User add(@RequestBody User newUser) {
        return userRepository.save(newUser);
    }

    @PutMapping(value = "/users", params = {"id"})
    User update(@RequestParam("id") String id, @RequestBody User updateData) {
        User user = userRepository.findById(id).orElse(null);
        if (user == null) {
            throw new UserNotFoundException("id:" + id);
        }
        if (updateData.getName() != null) user.setName(updateData.getName());
        if (updateData.getPassword() != null) user.setPassword(updateData.getPassword());
        userRepository.save(user);
        return user;
    }

    @DeleteMapping(value = "/users", params = {"id"})
    void delete(@RequestParam("id") String id) {
        User user = userRepository.findById(id).orElse(null);
        if (user == null) {
            throw new UserNotFoundException("id:" + id);
        }
        userRepository.delete(user);
    }
}
