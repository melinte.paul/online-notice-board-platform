package com.meep.noticeboard.bootstrap;

import com.meep.noticeboard.data.dirs.Directory;
import com.meep.noticeboard.data.dirs.DirectoryRepository;
import com.meep.noticeboard.data.files.File;
import com.meep.noticeboard.data.files.FileRepository;
import com.meep.noticeboard.data.users.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class DataBootStrap implements CommandLineRunner {
    private final UserRepository userRepository;
    private final DirectoryRepository directoryRepository;
    private final FileRepository fileRepository;

    public DataBootStrap(UserRepository userRepository, DirectoryRepository directoryRepository, FileRepository fileRepository) {
        this.userRepository = userRepository;
        this.directoryRepository = directoryRepository;
        this.fileRepository = fileRepository;
    }


    @Override
    public void run(String... args) throws Exception {
//        User u1 = new User("Ion","test");
//        userRepository.save(u1);
//        User u2 = new User("Paul","test2");
//        userRepository.save(u2);
//
//        Directory root = new Directory("root");
//        directoryRepository.save(root);
//
//        File f1 = new File("file1","Hello World!");
//        fileRepository.save(f1);
//        File f2 = new File("file2","Getting Started!");
//        fileRepository.save(f2);
//
//        Directory child1 = new Directory(root.getId(), new ArrayList<>(), new ArrayList<>(),"test1");
//        child1.getFileIds().add(f1.getId());
//        directoryRepository.save(child1);
//        Directory child2 = new Directory(root.getId(), new ArrayList<>(), new ArrayList<>(),"test2");
//        child2.getFileIds().add(f2.getId());
//        directoryRepository.save(child2);
//
//        root.getChildDirIds().add(child1.getId());
//        root.getChildDirIds().add(child2.getId());
//        directoryRepository.save(root);

    }
}
