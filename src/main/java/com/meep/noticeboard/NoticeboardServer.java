package com.meep.noticeboard;

import com.meep.noticeboard.data.users.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class NoticeboardServer {

    @GetMapping("/home")
    String users(){
        return "Hello World!";
    }

    public static void main(String[] args) {
        SpringApplication.run(NoticeboardServer.class, args);
    }

}
